#!/bin/bash

usage()
{
cat << EOF
usage: $0 options

This script creates a BitBucket repository.

OPTIONS:
   -h      Show this message
   -n      Repository name (default is current directory name). Spaces will be replaced with underscores.
   -u      BitBucket username
   -p      BitBucket password
   -s      SCM (git or hg, default git)
   -x      Do not perform an action after creating repo (if dir is already a repo, will be pushed, otherwise will be cloned)
   -o      Make the repo public
EOF
}

NAME="${PWD##*/}" #default is current working directory
SCM="git"
ACTION="true"
USER=
PASS=
PRIVATE="True"
while getopts "hoxn:s:u:p:" OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         n)
             NAME=$OPTARG
             ;;
         u)
             USER=$OPTARG
             ;;
         p)
             PASS=$OPTARG
             ;;
         s)
             SCM=$OPTARG
             ;;
         x)
         	 ACTION="false"
         	 ;;
         o)
         	 PRIVATE="False"
         	 ;;
         ?)
             usage
             exit
             ;;
     esac
done

if [ -z "$USER" ] || [ -z "$PASS" ]; then
    if [ -f "$HOME/.bitbucket" ]; then 
        echo "config file found"
        while IFS='= ' read var val
        do
            if [[ $var == \[*] ]]
            then
                section=$var
            elif [[ $val ]]
            then
                declare "config_$var=$val"
            fi
        done < $HOME/.bitbucket
        if [ -z "$config_username" ] || [ -z "$config_password" ]; then
            echo "Username and password are required"
            exit 1
        else
            USER=$config_username
            PASS=$config_password
        fi
    else 
        echo "Username and password are required"
	    exit 1
    fi
fi

# remove spaces in name
NAME=`echo "$NAME" | sed 's/ /_/g'`

if [ -d ".hg" ]; then
	echo "Repo type set to hg after .hg discovered"
	SCM="hg"
elif [ -d ".git" ]; then
	echo "Repo type set to git after .git discovered"
	SCM="git"
fi

echo "Creating repository..."
curl -s --request POST --user $USER:$PASS https://api.bitbucket.org/1.0/repositories/ --data name=$NAME --data scm=$SCM --data is_private=$PRIVATE > /dev/null

if [ "$ACTION" == "true" ]; then
	if [ -d ".hg" ]; then # this is a current hg repo
		echo "hg repo found. Pushing..."
		# hg requires edits to the .hgrc file to add a remote
		if [ ! -f ".hg/hgrc" ]; then # no hgrc file
			echo -e "\r\n[paths]\r\nbb=ssh://hg@bitbucket.org/$USER/${NAME,,}" >> .hg/hgrc
		elif ! grep "\[paths\]" .hg/hgrc > /dev/null; then # hgrc file with no paths
			echo -e "\r\n[paths]\r\nbb=ssh://hg@bitbucket.org/$USER/${NAME,,}" >> .hg/hgrc
		elif ! grep "ssh:\/\/hg@bitbucket.org\/$USER\/${NAME,,}" .hg/hgrc > /dev/null; then # hgrc file with paths, but not bb
			sed -i "s/\[paths\]/\[paths\]\r\nbb=ssh:\/\/hg@bitbucket.org\/$USER\/${NAME,,}/" .hg/hgrc
		fi
		hg push bb
	elif [ -d ".git" ]; then # this is a current git repo
		echo "git repo found. Pushing..."
		git remote add bb git@bitbucket.org:$USER/${NAME,,}.git
		git push bb master
	else # this is not a repo
		echo "No repo found. Cloning..."
		case $SCM in
		git)
			git clone git@bitbucket.org:$USER/${NAME,,}.git
			;;
		hg)
			hg clone ssh://hg@bitbucket.org/$USER/${NAME,,}
			;;
	esac
	fi
fi