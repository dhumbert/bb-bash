# bb-bash
### bash interface for Bitbucket

bb-bash is a simple Bash interface for Bitbucket. It currently allows you to create a Mercurial or Git repo on BB, and will either clone it to a directory, or, if the current directory is a working copy, it will push the current working copy to the new repo.

Instead of having to type your username and password each time, you can store your credentials in ~/.bitbucket in the following format:

    [auth]
    username = <Your Username>
    password = <Your Password

OPTIONS:  

    -h      Show this message
    -n      Repository name (default is current directory name). Spaces will be replaced with underscores.  
    -u      BitBucket username  
    -p      BitBucket password  
    -s      SCM (git or hg, default git)  
    -x      Do not perform an action after creating repo (if dir is already a repo, will be pushed, otherwise will be cloned)  
    -o      Make the repo public  
    
   
bb-bash was inspired by [bitbucket-cli](https://bitbucket.org/zhemao/bitbucket-cli).